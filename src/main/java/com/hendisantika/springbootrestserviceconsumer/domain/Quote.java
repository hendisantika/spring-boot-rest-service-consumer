package com.hendisantika.springbootrestserviceconsumer.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-rest-service-consumer
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/12/19
 * Time: 16.54
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Quote {

    private String type;
    private Value value;
}


